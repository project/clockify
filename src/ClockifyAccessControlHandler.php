<?php

namespace Drupal\clockify;

use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;

/**
 * Access controller for the Clockify entity.
 *
 * @see \Drupal\clockify\Entity\Clockify.
 */
class ClockifyAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var \Drupal\clockify\Entity\ClockifyInterface $entity */
    switch ($operation) {
      case 'view':
        if (!$entity->isPublished()) {
          return AccessResult::allowedIfHasPermission($account, 'view unpublished clockify entities');
        }
        return AccessResult::allowedIfHasPermission($account, 'view published clockify entities');

      case 'update':
        return AccessResult::allowedIfHasPermission($account, 'edit clockify entities');

      case 'delete':
        return AccessResult::allowedIfHasPermission($account, 'delete clockify entities');
    }

    // Unknown operation, no opinion.
    return AccessResult::neutral();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermission($account, 'add clockify entities');
  }

}
