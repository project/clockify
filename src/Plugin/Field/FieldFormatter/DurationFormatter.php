<?php

namespace Drupal\clockify\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Field\FieldItemListInterface;

/**
 * Plugin implementation of the 'duration_field_formatter' formatter.
 *
 * @FieldFormatter(
 *   id = "duration_field_formatter",
 *   label = @Translation("Duration Display field formatter"),
 *   field_types = {
 *     "field_duration"
 *   }
 * )
 */
class DurationFormatter extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = [];
    $summary[] = $this->t('Date formatter for duration.');
    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $element = [];
    foreach ($items as $delta => $item) {
      $dt = new \DateTime();
      $duration = $item->value;
      $dt->add(new \DateInterval($duration));
      $interval = $dt->diff(new \DateTime());
      $formatted_duration = ($interval->d * 24) + $interval->h . 'h ' . $interval->i . 'm ' . $interval->s . 's';
      $element[$delta] = ['#markup' => $formatted_duration];
    }
    return $element;
  }

  /**
   * Generate the output appropriate for one field item.
   *
   * @param \Drupal\Core\Field\FieldItemInterface $item
   *   One field item.
   *
   * @return string
   *   The textual output generated.
   */
  protected function viewValue(FieldItemInterface $item) {
    // The text value has no text format assigned to it, so the user input
    // should equal the output, including newlines.
    return nl2br(Html::escape($item->value));
  }

}
