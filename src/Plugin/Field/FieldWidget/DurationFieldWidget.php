<?php

namespace Drupal\clockify\Plugin\Field\FieldWidget;

use Drupal\Component\Utility\Color;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'duration_field_widget' widget.
 *
 * @FieldWidget(
 *   id = "duration_field_widget",
 *   module = "clockify",
 *   label = @Translation("Duration field widget, values as PT8H24M32S"),
 *   field_types = {
 *     "field_duration"
 *   }
 * )
 */
class DurationFieldWidget extends WidgetBase {

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $value = isset($items[$delta]->value) ? $items[$delta]->value : '';
    $element += [
      '#type' => 'textfield',
      '#default_value' => $value,
      '#size' => 7,
      '#maxlength' => 7,
    ];
    return ['value' => $element];
  }

}
