<?php

namespace Drupal\clockify;

use Drupal\Core\Entity\ContentEntityStorageInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\clockify\Entity\ClockifyInterface;

/**
 * Defines the storage handler class for Clockify entities.
 *
 * This extends the base storage class, adding required special handling for
 * Clockify entities.
 *
 * @ingroup clockify
 */
interface ClockifyStorageInterface extends ContentEntityStorageInterface {

  /**
   * Gets a list of Clockify revision IDs for a specific Clockify.
   *
   * @param \Drupal\clockify\Entity\ClockifyInterface $entity
   *   The Clockify entity.
   *
   * @return int[]
   *   Clockify revision IDs (in ascending order).
   */
  public function revisionIds(ClockifyInterface $entity);

  /**
   * Gets a list of revision IDs having a given user as Clockify author.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The user entity.
   *
   * @return int[]
   *   Clockify revision IDs (in ascending order).
   */
  public function userRevisionIds(AccountInterface $account);

  /**
   * Counts the number of revisions in the default language.
   *
   * @param \Drupal\clockify\Entity\ClockifyInterface $entity
   *   The Clockify entity.
   *
   * @return int
   *   The number of revisions in the default language.
   */
  public function countDefaultLanguageRevisions(ClockifyInterface $entity);

  /**
   * Unsets the language for all Clockify with the given language.
   *
   * @param \Drupal\Core\Language\LanguageInterface $language
   *   The language object.
   */
  public function clearRevisionsLanguage(LanguageInterface $language);

}
