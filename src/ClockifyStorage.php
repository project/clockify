<?php

namespace Drupal\clockify;

use Drupal\Core\Entity\Sql\SqlContentEntityStorage;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\clockify\Entity\ClockifyInterface;

/**
 * Defines the storage handler class for Clockify entities.
 *
 * This extends the base storage class, adding required special handling for
 * Clockify entities.
 *
 * @ingroup clockify
 */
class ClockifyStorage extends SqlContentEntityStorage implements ClockifyStorageInterface {

  /**
   * {@inheritdoc}
   */
  public function revisionIds(ClockifyInterface $entity) {
    return $this->database->query(
      'SELECT vid FROM {clockify_revision} WHERE id=:id ORDER BY vid',
      [':id' => $entity->id()]
    )->fetchCol();
  }

  /**
   * {@inheritdoc}
   */
  public function userRevisionIds(AccountInterface $account) {
    return $this->database->query(
      'SELECT vid FROM {clockify_field_revision} WHERE uid = :uid ORDER BY vid',
      [':uid' => $account->id()]
    )->fetchCol();
  }

  /**
   * {@inheritdoc}
   */
  public function countDefaultLanguageRevisions(ClockifyInterface $entity) {
    return $this->database->query('SELECT COUNT(*) FROM {clockify_field_revision} WHERE id = :id AND default_langcode = 1', [':id' => $entity->id()])
      ->fetchField();
  }

  /**
   * {@inheritdoc}
   */
  public function clearRevisionsLanguage(LanguageInterface $language) {
    return $this->database->update('clockify_revision')
      ->fields(['langcode' => LanguageInterface::LANGCODE_NOT_SPECIFIED])
      ->condition('langcode', $language->getId())
      ->execute();
  }

}
