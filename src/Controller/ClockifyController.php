<?php

namespace Drupal\clockify\Controller;

use Drupal\Component\Utility\Xss;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Datetime\DateFormatter;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Render\Renderer;
use Drupal\Core\Url;
use Drupal\clockify\Entity\ClockifyInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class ClockifyController.
 *
 *  Returns responses for Clockify routes.
 */
class ClockifyController extends ControllerBase implements ContainerInjectionInterface {


  /**
   * The date formatter.
   *
   * @var \Drupal\Core\Datetime\DateFormatter
   */
  protected $dateFormatter;

  /**
   * The renderer.
   *
   * @var \Drupal\Core\Render\Renderer
   */
  protected $renderer;

  /**
   * Constructs a new ClockifyController.
   *
   * @param \Drupal\Core\Datetime\DateFormatter $date_formatter
   *   The date formatter.
   * @param \Drupal\Core\Render\Renderer $renderer
   *   The renderer.
   */
  public function __construct(DateFormatter $date_formatter, Renderer $renderer) {
    $this->dateFormatter = $date_formatter;
    $this->renderer = $renderer;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('date.formatter'),
      $container->get('renderer')
    );
  }

  /**
   * Displays a Clockify revision.
   *
   * @param int $clockify_revision
   *   The Clockify revision ID.
   *
   * @return array
   *   An array suitable for drupal_render().
   */
  public function revisionShow($clockify_revision) {
    $clockify = $this->entityTypeManager()->getStorage('clockify')
      ->loadRevision($clockify_revision);
    $view_builder = $this->entityTypeManager()->getViewBuilder('clockify');

    return $view_builder->view($clockify);
  }

  /**
   * Page title callback for a Clockify revision.
   *
   * @param int $clockify_revision
   *   The Clockify revision ID.
   *
   * @return string
   *   The page title.
   */
  public function revisionPageTitle($clockify_revision) {
    $clockify = $this->entityTypeManager()->getStorage('clockify')
      ->loadRevision($clockify_revision);
    return $this->t('Revision of %title from %date', [
      '%title' => $clockify->label(),
      '%date' => $this->dateFormatter->format($clockify->getRevisionCreationTime()),
    ]);
  }

  /**
   * Generates an overview table of older revisions of a Clockify.
   *
   * @param \Drupal\clockify\Entity\ClockifyInterface $clockify
   *   A Clockify object.
   *
   * @return array
   *   An array as expected by drupal_render().
   */
  public function revisionOverview(ClockifyInterface $clockify) {
    $account = $this->currentUser();
    $clockify_storage = $this->entityTypeManager()->getStorage('clockify');

    $langcode = $clockify->language()->getId();
    $langname = $clockify->language()->getName();
    $languages = $clockify->getTranslationLanguages();
    $has_translations = (count($languages) > 1);
    $build['#title'] = $has_translations ? $this->t('@langname revisions for %title', ['@langname' => $langname, '%title' => $clockify->label()]) : $this->t('Revisions for %title', ['%title' => $clockify->label()]);

    $header = [$this->t('Revision'), $this->t('Operations')];
    $revert_permission = (($account->hasPermission("revert all clockify revisions") || $account->hasPermission('administer clockify entities')));
    $delete_permission = (($account->hasPermission("delete all clockify revisions") || $account->hasPermission('administer clockify entities')));

    $rows = [];

    $vids = $clockify_storage->revisionIds($clockify);

    $latest_revision = TRUE;

    foreach (array_reverse($vids) as $vid) {
      /** @var \Drupal\clockify\ClockifyInterface $revision */
      $revision = $clockify_storage->loadRevision($vid);
      // Only show revisions that are affected by the language that is being
      // displayed.
      if ($revision->hasTranslation($langcode) && $revision->getTranslation($langcode)->isRevisionTranslationAffected()) {
        $username = [
          '#theme' => 'username',
          '#account' => $revision->getRevisionUser(),
        ];

        // Use revision link to link to revisions that are not active.
        $date = $this->dateFormatter->format($revision->getRevisionCreationTime(), 'short');
        if ($vid != $clockify->getRevisionId()) {
          $link = $this->l($date, new Url('entity.clockify.revision', [
            'clockify' => $clockify->id(),
            'clockify_revision' => $vid,
          ]));
        }
        else {
          $link = $clockify->link($date);
        }

        $row = [];
        $column = [
          'data' => [
            '#type' => 'inline_template',
            '#template' => '{% trans %}{{ date }} by {{ username }}{% endtrans %}{% if message %}<p class="revision-log">{{ message }}</p>{% endif %}',
            '#context' => [
              'date' => $link,
              'username' => $this->renderer->renderPlain($username),
              'message' => [
                '#markup' => $revision->getRevisionLogMessage(),
                '#allowed_tags' => Xss::getHtmlTagList(),
              ],
            ],
          ],
        ];
        $row[] = $column;

        if ($latest_revision) {
          $row[] = [
            'data' => [
              '#prefix' => '<em>',
              '#markup' => $this->t('Current revision'),
              '#suffix' => '</em>',
            ],
          ];
          foreach ($row as &$current) {
            $current['class'] = ['revision-current'];
          }
          $latest_revision = FALSE;
        }
        else {
          $links = [];
          if ($revert_permission) {
            $links['revert'] = [
              'title' => $this->t('Revert'),
              'url' => $has_translations ?
              Url::fromRoute('entity.clockify.translation_revert', [
                'clockify' => $clockify->id(),
                'clockify_revision' => $vid,
                'langcode' => $langcode,
              ]) :
              Url::fromRoute('entity.clockify.revision_revert', [
                'clockify' => $clockify->id(),
                'clockify_revision' => $vid,
              ]),
            ];
          }

          if ($delete_permission) {
            $links['delete'] = [
              'title' => $this->t('Delete'),
              'url' => Url::fromRoute('entity.clockify.revision_delete', [
                'clockify' => $clockify->id(),
                'clockify_revision' => $vid,
              ]),
            ];
          }

          $row[] = [
            'data' => [
              '#type' => 'operations',
              '#links' => $links,
            ],
          ];
        }

        $rows[] = $row;
      }
    }

    $build['clockify_revisions_table'] = [
      '#theme' => 'table',
      '#rows' => $rows,
      '#header' => $header,
    ];

    return $build;
  }

}
