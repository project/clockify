<?php

namespace Drupal\clockify;

use Drupal\user\Entity\User;
use Drupal\Component\Utility\UrlHelper;
use GuzzleHttp\Exception\ClientException;
use Drupal\taxonomy\Entity\Term;

/**
 * TODO: complete this.
 */
class ClockifyController {

  private $globalApi;
  private $apiUrl = 'https://api.clockify.me/api/v1/';

  /**
   *
   */
  public function __construct($request_from = FALSE) {
    $config = \Drupal::config('clockify.settings');
    $this->globalApi = $config->get('api_key');
    $this->client = \Drupal::httpClient();
    $this->loadFrom = $request_from;

    // $this->sqlQuery = $sql_query;
    // $this->entityManager = \Drupal::entityManager();
  }

  /**
   * Loads workspaces accessible for the configured user.
   *
   * @return array
   *   Array containing the workspaces and its members.
   */
  public function getWorkspaces() {
    return $this->apiCall('workspaces/');
  }

  /**
   * Gets members form api for given workspace.
   *
   * @param string $workspace_id
   *   The workspace id used on clockify.
   *
   * @return array
   *   Contains the members and some basic information from them.
   */
  public function getMembers($workspace_id) {
    return $this->apiCall('workspaces/' . $workspace_id . '/users');
  }

  /**
   * Gets tags for given workspace.
   *
   * @param string $workspace_id
   *   The workspace id used on Clockify.
   */
  public function getTags($workspace_id) {
    $tags = $this->apiCall('/workspaces/' . $workspace_id . '/tags');
    foreach ($tags as $key => $value) {
      $query = \Drupal::entityQuery('taxonomy_term');
      $query->condition('field_clockify_id', $value->id);
      $result = $query->execute();

      if (!$result) {
        $query = \Drupal::entityQuery('clockify');
        $query->condition('type', 'workspace');
        $query->condition('field_clockify_id', $value->workspaceId);
        $workspace = $query->execute();
        $workspace = reset($workspace);
        $term = Term::create([
          'name' => $value->name,
          'vid' => 'clockify_tags',
          'field_workspace' => [['target_id' => $workspace]],
          'field_clockify_id' => $value->id,
        ])->save();
      }

    }
  }

  /**
   * Loads the projects for any given workspace.
   *
   * @param string $workspace_id
   *   The workspace id used on clockify.
   *
   * @return array
   *   Contains information about the project.
   */
  public function getProjects($workspace_id) {
    return $this->apiCall('workspaces/' . $workspace_id . '/projects');
  }

  /**
   * Loads projects for the given workspaces.
   */
  public function saveWorkspaces() {
    $workspaces = $this->getWorkspaces();
    foreach ($workspaces as $workspace) {
      $members = $this->saveMembers($workspace->id);
      $this->getTags($workspace->id);
      $query = \Drupal::entityQuery('clockify');
      $query->condition('type', 'workspace');
      $query->condition('field_clockify_id', $workspace->id);
      $result = $query->execute();
      if (!$result) {
        $values = [
          'type' => 'workspace',
          'name' => $workspace->name,
          'field_clockify_id' => $workspace->id,
          'field_members' => $members,
        ];
        $ws = entity_create('clockify', $values);
        $ws->save();
        $query = \Drupal::entityQuery('clockify');
        $query->condition('field_clockify_id', $workspace->id);
        $result = $query->execute();
        $id = reset($result);
        $this->saveProjects($workspace->id, $ws->id());
      }
      else {
        $this->saveProjects($workspace->id, reset($result));
      }
      foreach ($workspace->memberships as $member) {
        $this->saveTimeEntry($workspace->id, $member->userId);
      }
    }
  }

  /**
   * Saves projects returnd by the api.
   *
   * @param string $workspace_id
   *   The clockify if of the workspace.
   * @param string $wid
   *   The drupal id of the workspace.
   */
  public function saveProjects($workspace_id, $wid) {
    $projects = $this->getProjects($workspace_id);
    foreach ($projects as $project) {
      $query = \Drupal::entityQuery('clockify');
      $query->condition('type', 'project');
      $query->condition('field_clockify_id', $project->id);
      $result = $query->execute();
      if (!reset($result)) {
        $members = $this->loadMembers($project->memberships);
        $values = [
          'type' => 'project',
          'field_clockify_id' => $project->id,
          'name' => $project->name,
          'field_client_name' => $project->clientName,
          'field_color' => $project->color,
          'field_workspace' => $wid,
          'field_members' => $members,
        ];
        $pr = entity_create('clockify', $values);
        $pr->save();
      }

    }
  }

  /**
   * Loads members for a given project.
   *
   * @param array $members
   *   Array containing the clockify user ids of members.
   *
   * @return array
   *   Array containing the user ids of members.
   */
  public function loadMembers(array $members) {
    $uids = [];
    foreach ($members as $member) {
      $query = \Drupal::entityQuery('user');
      $query->condition('field_clockify_id', $member->userId);
      $result = $query->execute();
      if (count($result) > 0) {
        $uids[] = reset($result);
      }
    }
    return $uids;
  }

  /**
   * Loads and saves useres for the given workspace.
   *
   * @param string $workspace_id
   *   The ID of the workspace.
   *
   * @return array
   *   Array containing the user ids for the given workspace.
   */
  public function saveMembers($workspace_id) {
    $users = [];
    $members = $this->getMembers($workspace_id);
    foreach ($members as $member) {
      $query = \Drupal::entityQuery('user');
      $query->condition('field_clockify_id', $member->id);
      $entity_ids = $query->execute();
      if (count($entity_ids) === 1) {
        $users[] = reset($entity_ids);
      }
      else {
        $query = \Drupal::entityQuery('user');
        $query->condition('mail', $member->email);
        $entity_ids = $query->execute();
        if (count($entity_ids) === 1) {
          $uid = reset($entity_ids);
          $user = User::load($uid);
          $user->set('field_clockify_id', $member->id);
          $user->save();
          $users[] = $user->id();
        }
        else {
          $values = [
            'name' => $member->name,
            'mail' => $member->email,
            'field_clockify_id' => $member->id,
            'status' => 1,
          ];
          $account = entity_create('user', $values);
          $account->save();
          $users[] = $account->id();
        }
      }
    }
    return $users;
  }

  /**
   * Gathers and saves time entryes.
   *
   * @param string $workspace_id
   *   The workspace id to gather from.
   * @param string $member_id
   *   The member id to gather from.
   */
  public function saveTimeEntry($workspace_id, $member_id) {
    $path = 'workspaces/' . $workspace_id . '/user/' . $member_id . '/time-entries';
    $page = 1;
    $page_size = 20;
    if ($this->loadFrom) {
      $parameters = [
        'start' => $this->loadFrom,
        'page-size' => $page_size,
        'page' => $page,
      ];
      $query_parameters = UrlHelper::buildQuery($parameters);
      $path = $path . '?' . $query_parameters;
    }
    do {
      $time_tracking = $this->apiCall($path);
      // dpm($time_tracking);
      foreach ($time_tracking as $time) {
        $query = \Drupal::entityQuery('clockify');
        $query->condition('type', 'time_entry');
        $query->condition('field_clockify_id', $time->id);
        $result = $query->execute();

        $start = strtotime($time->timeInterval->start);
        $start = format_date($start, 'custom', DATETIME_DATETIME_STORAGE_FORMAT);
        $end = strtotime($time->timeInterval->end);
        if (is_numeric($end)) {
          $end = format_date($end, 'custom', DATETIME_DATETIME_STORAGE_FORMAT);
        }
        else {
          $end = '';
        }
        $tags = [];
        foreach ($time->tagIds as $tag) {
          $tag_query = \Drupal::entityQuery('taxonomy_term');
          $tag_query->condition('field_clockify_id', $tag);
          $tag_result = $tag_query->execute();
          if ($tag_result) {
            $tags[] = ['target_id' => reset($tag_result)];
          }
        }
        if (!$result) {
          $time_owner = '';
          $user = \Drupal::entityQuery('user');
          $user->condition('field_clockify_id', $time->userId);
          $user = $user->execute();
          if (reset($user)) {
            $time_owner = reset($user);
          }

          $values = [
            'type' => 'time_entry',
            'name' => substr($time->description, 0, 15),
            'field_description' => $time->description,
            'field_clockify_id' => $time->id,
            'field_start_time' => $start,
            'field_end_time' => $end,
            'field_workspace' => $this->getEntity('workspace', $time->workspaceId),
            'field_project' => $time->projectId ? $this->getEntity('project', $time->projectId) : '',
            'field_owner' => $time_owner,
            'field_duration' => $time->timeInterval->duration,
            'field_tags' => $tags ? $tags : [],
          ];
          $tr = entity_create('clockify', $values);
          $tr->save();
        }
        else {
          $entry = \Drupal::entityTypeManager()->getStorage('clockify')->load(reset($result));
          $entry->set('field_start_time', $start);
          $entry->set('field_end_time', $end);
          $entry->set('field_duration', $time->timeInterval->duration);
          if ($tags) {
            $entry->set('field_tags', $tags);
          }
          $entry->save();
        }
      }
    } while (count($time_tracking) === $page_size);
  }

  /**
   * Load entity of type with id.
   *
   * @param string $type
   *   The type of the entity, ex: workspace, project or time_entry.
   * @param string $clockify_id
   *   The clockify id of the given content.
   *
   * @return sting
   *   The drupal entity id of the content.
   */
  public function getEntity(string $type, string $clockify_id) {
    $query = \Drupal::entityQuery('clockify');
    $query->condition('type', $type);
    $query->condition('field_clockify_id', $clockify_id);
    $result = $query->execute();
    if (count($result) > 0) {
      return reset($result);
    }
    else {
      return '';
    }
  }

  /**
   * Loads members drupal ids.
   *
   * @param array $members
   *   Array containing the members returned by the clockify api.
   *
   * @return array
   *   Contains the drupal ids of members.
   */
  public function getMemberIds($members) {
    $ids = [];
    foreach ($members as $member) {
      $query = \Drupal::entityQuery('user');
      $query->condition('field_clockify_id', $member->id);
      $uid = $query->execute();
      $ids[] = reset($uid);
    }
    return $ids;
  }

  /**
   * Calls the clockify api.
   *
   * @param string $url
   *  Contains the endpoint to call, ex /workspaces/{workspace_id}.
   *
   * @param string $method
   *  Contains the method for the api call.
   * 
   * @return array
   *  Returns the decoded response from the api call.
   */
  private function apiCall($url, $method = 'GET') {
    try {
      $path = $this->apiUrl . $url;
      $request = $this->client->request($method, $path, [
        'headers' => ['X-Api-Key' => $this->globalApi],
      ]);
      $file_contents = $request->getBody()->getContents();
      $result = json_decode($file_contents);
    }
    catch (ClientException $exception) {
      $result = [];
    }
    return $result;
  }

}
