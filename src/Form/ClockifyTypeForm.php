<?php

namespace Drupal\clockify\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class ClockifyTypeForm.
 */
class ClockifyTypeForm extends EntityForm {

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    $clockify_type = $this->entity;
    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#maxlength' => 255,
      '#default_value' => $clockify_type->label(),
      '#description' => $this->t("Label for the Clockify type."),
      '#required' => TRUE,
    ];

    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $clockify_type->id(),
      '#machine_name' => [
        'exists' => '\Drupal\clockify\Entity\ClockifyType::load',
      ],
      '#disabled' => !$clockify_type->isNew(),
    ];

    /* You will need additional form elements for your custom properties. */

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $clockify_type = $this->entity;
    $status = $clockify_type->save();

    switch ($status) {
      case SAVED_NEW:
        $this->messenger()->addMessage($this->t('Created the %label Clockify type.', [
          '%label' => $clockify_type->label(),
        ]));
        break;

      default:
        $this->messenger()->addMessage($this->t('Saved the %label Clockify type.', [
          '%label' => $clockify_type->label(),
        ]));
    }
    $form_state->setRedirectUrl($clockify_type->toUrl('collection'));
  }

}
