<?php

namespace Drupal\clockify\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class ClockifySettingsForm.
 *
 * @ingroup clockify
 */
class ClockifySettingsForm extends ConfigFormBase {

  const SETTINGS = 'clockify.settings';

  /**
   * Returns a unique string identifying the form.
   *
   * @return string
   *   The unique string identifying the form.
   */
  public function getFormId() {
    return 'clockify_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      static::SETTINGS,
    ];
  }

  /**
   * Form submission handler.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Empty implementation of the abstract submit class.
    $this->configFactory->getEditable(static::SETTINGS)
      ->set('api_key', $form_state->getValue('clockify_admin_key'))
      ->set('interval', $form_state->getValue('cron_interval'))
      ->save();
    parent::submitForm($form, $form_state);
  }

  /**
   * Defines the settings form for Clockify entities.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @return array
   *   Form definition array.
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config(static::SETTINGS);

    $form['clockify_settings']['#markup'] = $this->t('Settings form for Clockify Api.');
    $form['clockify_admin_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Clockify api key'),
      '#description' => $this->t('The admin users api key found at the end of this <a href="@link">page</a>.', ['@link' => 'https://clockify.me/user/settings']),
      '#default_value' => $config->get('api_key'),
    ];
    $form['cron_interval'] = [
      '#type' => 'select',
      '#title' => $this->t('Cron interval'),
      '#description' => $this->t('Time after which clockify cron will pull new data. Will only query for items added after each cron.'),
      '#default_value' => $config->get('interval'),
      '#options' => [
        60 => $this->t('1 minute'),
        300 => $this->t('5 minutes'),
        3600 => $this->t('1 hour'),
        10800 => $this->t('3 hours'),
        21600 => $this->t('6 hours'),
        43200 => $this->t('12 hours'),
        86400 => $this->t('1 day'),
        604800 => $this->t('7 days'),
      ],
    ];

    $form['sync'] = [
      '#type' => 'details',
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
      '#description' => $this->t('Save the form with a valid API key before clicking this button'),
      '#title' => $this->t('Sync data'),
    ];

    $form['sync']['run_syn'] = [
      '#type' => 'submit',
      '#value' => $this->t('Sync all data from clockify'),
      '#submit' => [[$this, 'syncData']],
    ];

    $form['sync']['clockify_message'] = [
      '#type' => 'item',
      '#markup' => $this->t(''),
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * Call the sync function from the module file.
   */
  public function syncData() {
    clockify_sync();
  }
}
