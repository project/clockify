<?php

namespace Drupal\clockify\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Clockify entities.
 *
 * @ingroup clockify
 */
class ClockifyDeleteForm extends ContentEntityDeleteForm {


}
