<?php

namespace Drupal\clockify\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\RevisionLogInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\Core\Entity\EntityPublishedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining Clockify entities.
 *
 * @ingroup clockify
 */
interface ClockifyInterface extends ContentEntityInterface, RevisionLogInterface, EntityChangedInterface, EntityPublishedInterface, EntityOwnerInterface {

  /**
   * Add get/set methods for your configuration properties here.
   */

  /**
   * Gets the Clockify name.
   *
   * @return string
   *   Name of the Clockify.
   */
  public function getName();

  /**
   * Sets the Clockify name.
   *
   * @param string $name
   *   The Clockify name.
   *
   * @return \Drupal\clockify\Entity\ClockifyInterface
   *   The called Clockify entity.
   */
  public function setName($name);

  /**
   * Gets the Clockify creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Clockify.
   */
  public function getCreatedTime();

  /**
   * Sets the Clockify creation timestamp.
   *
   * @param int $timestamp
   *   The Clockify creation timestamp.
   *
   * @return \Drupal\clockify\Entity\ClockifyInterface
   *   The called Clockify entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Gets the Clockify revision creation timestamp.
   *
   * @return int
   *   The UNIX timestamp of when this revision was created.
   */
  public function getRevisionCreationTime();

  /**
   * Sets the Clockify revision creation timestamp.
   *
   * @param int $timestamp
   *   The UNIX timestamp of when this revision was created.
   *
   * @return \Drupal\clockify\Entity\ClockifyInterface
   *   The called Clockify entity.
   */
  public function setRevisionCreationTime($timestamp);

  /**
   * Gets the Clockify revision author.
   *
   * @return \Drupal\user\UserInterface
   *   The user entity for the revision author.
   */
  public function getRevisionUser();

  /**
   * Sets the Clockify revision author.
   *
   * @param int $uid
   *   The user ID of the revision author.
   *
   * @return \Drupal\clockify\Entity\ClockifyInterface
   *   The called Clockify entity.
   */
  public function setRevisionUserId($uid);

}
