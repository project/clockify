<?php

namespace Drupal\clockify\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBundleBase;

/**
 * Defines the Clockify type entity.
 *
 * @ConfigEntityType(
 *   id = "clockify_type",
 *   label = @Translation("Clockify type"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\clockify\ClockifyTypeListBuilder",
 *     "form" = {
 *       "add" = "Drupal\clockify\Form\ClockifyTypeForm",
 *       "edit" = "Drupal\clockify\Form\ClockifyTypeForm",
 *       "delete" = "Drupal\clockify\Form\ClockifyTypeDeleteForm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\clockify\ClockifyTypeHtmlRouteProvider",
 *     },
 *   },
 *   config_prefix = "clockify_type",
 *   admin_permission = "administer site configuration",
 *   bundle_of = "clockify",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid"
 *   },
 *   links = {
 *     "canonical" = "/admin/structure/clockify/clockify_type/{clockify_type}",
 *     "add-form" = "/admin/structure/clockify/clockify_type/add",
 *     "edit-form" = "/admin/structure/clockify/clockify_type/{clockify_type}/edit",
 *     "delete-form" = "/admin/structure/clockify/clockify_type/{clockify_type}/delete",
 *     "collection" = "/admin/structure/clockify/clockify_type"
 *   }
 * )
 */
class ClockifyType extends ConfigEntityBundleBase implements ClockifyTypeInterface {

  /**
   * The Clockify type ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The Clockify type label.
   *
   * @var string
   */
  protected $label;

}
