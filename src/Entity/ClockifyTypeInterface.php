<?php

namespace Drupal\clockify\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface for defining Clockify type entities.
 */
interface ClockifyTypeInterface extends ConfigEntityInterface {

  // Add get/set methods for your configuration properties here.
}
