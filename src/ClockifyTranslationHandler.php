<?php

namespace Drupal\clockify;

use Drupal\content_translation\ContentTranslationHandler;

/**
 * Defines the translation handler for clockify.
 */
class ClockifyTranslationHandler extends ContentTranslationHandler {

  // Override here the needed methods from ContentTranslationHandler.
}
