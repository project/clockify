<?php

/**
 * @file
 * Contains clockify.page.inc.
 *
 * Page callback for Clockify entities.
 */

use Drupal\Core\Render\Element;

/**
 * Prepares variables for Clockify templates.
 *
 * Default template: clockify.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_clockify(array &$variables) {
  // Fetch Clockify Entity Object.
  $clockify = $variables['elements']['#clockify'];

  // Helpful $content variable for templates.
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}
